import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AutenticacionService } from '../servicios/autenticacion.service';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css'],
})
export class FormLoginComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  constructor(
    private autenticacionService: AutenticacionService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  mensaje: string = 'Si es un usuario registrado, inicie sesión aquí.';
  claseMensaje: string = 'text-white';

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]});
  }

  login(): void {
    if (this.form.valid) {
      this.autenticacionService.login(this.form.value).subscribe(
        (response) => {
          this.autenticacionService.setToken(response.token);
          this.router.navigateByUrl('/');
        },
        (error) => {
          console.log(error);
          this.mensaje = 'Se ha producido un error en la autenticación.';
          this.claseMensaje = 'text-danger';
        }
      );
    }
  }
}
