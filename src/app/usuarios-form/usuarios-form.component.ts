import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Usuario } from '../modelo/usuario';
import { UsuarioService } from '../servicios/usuario.service';

@Component({
  selector: 'app-usuarios-form',
  templateUrl: './usuarios-form.component.html',
  styleUrls: ['./usuarios-form.component.css'],
})
export class UsuariosFormComponent implements OnInit {
  usuario: Usuario = new Usuario();

  constructor(
    private rutaActiva: ActivatedRoute,
    private enrutador: Router,
    private usuarioService: UsuarioService) {
    console.log(this.usuario);
  }

  ngOnInit(): void {
    // Recuperando el parámetro
    this.rutaActiva.params.subscribe((params: Params) => {
      this.recuperarUsuario(params.id);
    });
  }

  private recuperarUsuario(id: number) {
    if (id >= 0) {
      this.usuarioService.recuperarUsuario(id).subscribe(
        (response) => {
          this.usuario = response.data;
        }
      );
    }
  }

  procesarUsuario(form: NgForm) {
    if (form.valid) {
      if(this.usuario.id==0){
        this.crearUsuario(form.value);
      }else{
        this.actualizarUsuario(form.value);
      }
    }

    // Navegar al listado de usuarios
    this.enrutador.navigateByUrl('/usuarios');
  }

  crearUsuario(usuario: Usuario){
    this.usuarioService.crearUsuario(usuario).subscribe();
  }

  actualizarUsuario(usuario: Usuario){
    this.usuarioService.actualizarUsuario(usuario).subscribe();
  }
}
