import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AutenticacionService } from './servicios/autenticacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'crud-app';
  defaultLanguage: string = 'es';
  constructor(
    public autenticacionService: AutenticacionService,
    private traduccion: TranslateService) {
    traduccion.setDefaultLang(this.defaultLanguage);
  }
}
