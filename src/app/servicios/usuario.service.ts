import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Usuario } from '../modelo/usuario';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  constructor(private httpClient: HttpClient) {}

  gestionErrores(error: any): any{
    return;
  }

  // recuperar usuarios de forma paginada
  recuperarUsuarios(numPagina: number): Observable<any> {
    const params = { page: numPagina };
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');

    return this.httpClient.get<any>('https://reqres.in/api/users', {
      headers: headers,
      params: params,
      observe: 'body',
      responseType: 'json',
    }).pipe(
      retry(1),
      catchError(this.gestionErrores)
    );
  }

  recuperarUsuario(id: number): Observable<any> {
      return this.httpClient.get<any>(`https://reqres.in/api/users/${id}`)
      .pipe(
        retry(1),
        catchError(this.gestionErrores));
  }

  borrarUsuario(id: number): Observable<any>{
    return this.httpClient.delete<any>(`https://reqres.in/api/users/${id}`)
    .pipe(
      retry(2),
      catchError(this.gestionErrores)
    );
  }

  crearUsuario(usuario: Usuario): Observable<any>{
    return this.httpClient.post<any>('https://reqres.in/api/users', usuario)
    .pipe(
      retry(1), catchError(this.gestionErrores)
    );
  }

  actualizarUsuario(usuario: Usuario): Observable<any>{
    return this.httpClient.put<any>(`https://reqres.in/api/users/${usuario.id}`, usuario)
    .pipe(
      retry(1), catchError(this.gestionErrores)
    );
  }
}
