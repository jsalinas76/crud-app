import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }

  login(user: any): Observable<any>{
    return this.httpClient.post('https://reqres.in/api/login', user);
  }

  setToken(token: string){
    this.cookieService.set('token', token);
  }

  getToken(): string{
    return this.cookieService.get('token');
  }

  isLogged():boolean{
    return this.cookieService.check('token');
  }

  logout(){
    this.cookieService.delete('token');
  }

}
