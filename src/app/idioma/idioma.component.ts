import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-idioma',
  templateUrl: './idioma.component.html',
  styleUrls: ['./idioma.component.css']
})
export class IdiomaComponent implements OnInit {

  constructor(private servicioTraduccion: TranslateService) { }

  ngOnInit(): void {
  }

  cambiarIdioma(idioma: string): void{
    this.servicioTraduccion.use(idioma);
  }

}
