import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FacturasComponent } from './facturas/facturas.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { AutenticacionGuard } from './guards/autenticacion.guard';
import { InicioComponent } from './inicio/inicio.component';
import { UsuariosFormComponent } from './usuarios-form/usuarios-form.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

const routes: Routes = [
  {path:'inicio', component: InicioComponent, canActivate: [AutenticacionGuard]},
  {path:'usuarios', component: UsuariosComponent, canActivate: [AutenticacionGuard]},
  {path:'usuarios/:id', component: UsuariosFormComponent, canActivate: [AutenticacionGuard]},
  {path:'facturas', component: FacturasComponent, canActivate: [AutenticacionGuard]},
  {path:'login', component: FormLoginComponent},
  {path:'', redirectTo: 'inicio', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
