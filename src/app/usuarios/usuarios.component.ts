import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../modelo/usuario';
import { UsuarioService } from '../servicios/usuario.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios = new Array<Usuario>();
  tamanoTotalPaginas: number = 0;

  constructor(private usuarioService: UsuarioService) {}

  borrarUsuario(id: number): void{
    this.usuarioService.borrarUsuario(id).subscribe();
  }

  cargarPagina(numPagina: number): void {

    this.usuarioService.recuperarUsuarios(numPagina)
      .subscribe(response => {
          this.usuarios = response.data;
          this.tamanoTotalPaginas = response.total_pages;
        }
      );
  }

  ngOnInit(): void {
    this.cargarPagina(1);
  }

  contador(i: number) {
    return new Array(i);
  }
}
